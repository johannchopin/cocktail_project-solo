<?php

require_once __DIR__ . '/../databaseHandler.php';

class Request
{
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseHandler(__DIR__ . '/../../credentials/database-credentials.json');

        $this->database->connect();
        $this->init();
        $this->database->disconnect();
    }

    protected function init(): void
    {
        echo json_encode($this->getAllIngredients());
    }

    protected function getAllIngredients(): array
    {
        $allIngredients = array();
        $ingredientsResponse = $this->database->get("SELECT nom FROM ingredient");

        foreach ($ingredientsResponse as $ingredient) {
            array_push($allIngredients, $ingredient['nom']);
        }

        return $allIngredients;
    }
}

new Request();
