<?php

require_once __DIR__ . '/databaseHandler.php';
require_once __DIR__ . '/helper.php';

class PageHandler
{
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseHandler(__DIR__ . '/../credentials/database-credentials.json');
        $this->database->connect();
    }

    protected function getTemplate(string $pathToTemplate, array $replacer = array()): string
    {
        $template = file_get_contents($pathToTemplate);

        foreach ($replacer as $replacerKey => $replacerValue) {
            $template = str_replace($replacerKey, $replacerValue, $template);
        }

        return $template;
    }

    protected function uniqidReal($lenght = 8)
    {
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($lenght / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }

        return substr(bin2hex($bytes), 0, $lenght);
    }

    protected function getGeneratedToken()
    {
        $id = self::uniqidReal();

        for ($i = 0; $i < 3; $i++) {
            $id .= '-' . self::uniqidReal();
        }

        return $id;
    }

    protected function isUserConnected(): bool
    {
        return $this->database->isPresent("SELECT id FROM client WHERE token = :userToken", array(
            "userToken" => $this->getCookie("token"),
        ));
    }

    protected function setCookie(string $cookieName, string $value): void
    {
        setcookie("CPS_" . $cookieName, $value);
    }

    protected function getCookie(string $cookieName)
    {
        if (isset($_COOKIE["CPS_" . $cookieName])) {
            return $_COOKIE["CPS_" . $cookieName];
        }

        return NULL;
    }

    public function getUserToken()
    {
        return $this->getCookie("token");
    }

    public function linkRender(string $link, string $linkContent = ""): string
    {
        $linkTemplate = '<a href="__LINK__">__CONTENT__</a>';

        return str_replace("__CONTENT__", $linkContent, str_replace("__LINK__", $link, $linkTemplate));
    }

    public function listRender(array $elements): string
    {
        $listRender = "";

        foreach ($elements as $element) {
            $listRender .= "<li>{$element}</li>";
        }

        return $listRender;
    }
}
