<?php

// DISPLAY PHP ERRORS
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);


// INIT PAGEHANDLER ZONE
require_once __DIR__ . '/core/pageHandler.php';
// INIT PAGEHANDLER ZONE


class Index extends PageHandler
{
    protected $selectedPage;
    protected $selectedPageHandler;

    public function __construct()
    {
        parent::__construct();

        $this->setSelectedPage();
        $this->setSelectedPageHandler();
        $this->pageRender();
    }

    protected function pageRender()
    {
        echo $this->getTemplate(__DIR__ . "/template.html", array(
            "__SPECIFIC_STYLES__"   => $this->getPathToSpecificStyles(),
            "__SPECIFIC_SCRIPT__"   => $this->getPathToSpecificScripts(),
            "__NAVBAR__"            => $this->navbarRender(),
            "__SIDEBAR__"           => $this->sidebarRender(),
            "__PAGE__"              => $this->selectedPageHandler->pageRender(),
        ));
    }

    protected function setSelectedPage(): void
    {
        if (isset($_GET["p"])) {
            $this->selectedPage = $_GET["p"];
        } else {
            $this->selectedPage = "home";
        }
    }

    protected function setSelectedPageHandler(): void
    {
        switch ($this->selectedPage) {
            default:
                require_once __DIR__ . '/pages/home/index.php';
                $this->selectedPageHandler = new HomePage();
                break;

            case "home":
                require_once __DIR__ . '/pages/home/index.php';
                $this->selectedPageHandler = new HomePage();
                break;

            case "cocktail":
                require_once __DIR__ . '/pages/cocktail/index.php';
                $this->selectedPageHandler = new CocktailPage();
                break;

            case "login":
                require_once __DIR__ . '/pages/login/index.php';
                $this->selectedPageHandler = new LoginPage();
                break;

            case "signin":
                require_once __DIR__ . '/pages/signin/index.php';
                $this->selectedPageHandler = new SigninPage();
                break;

            case "userSettings":
                require_once __DIR__ . '/pages/userSettings/index.php';
                $this->selectedPageHandler = new UserSettingsPage();
                break;
        }
    }

    protected function getPathToSpecificStyles(): string
    {
        return "./pages/{$this->selectedPage}/styles.css";
    }

    protected function getPathToSpecificScripts(): string
    {
        return "./pages/{$this->selectedPage}/script.js";
    }

    protected function navbarRender(): string
    {
        return $this->getTemplate(__DIR__ . "/assets/components/navbar.html", array(
            "__LINK_TO_SIGNIN_PAGE__" => "?p=signin",
            "__LINK_TO_LOGIN_PAGE__" => "?p=login",
            "__LINK_TO_USER_SETTINGS_PAGE__" => "?p=userSettings",
            "__USER_FEATURE_DISPLAY__" => $this->isUserConnected() ? 'inline-block' : 'none',
            "__SEARCHBAR_DISPLAY__"  => $this->isUserConnected() ? 'visible' : 'hidden',
        ));
    }

    protected function sidebarRender()
    {
        return $this->getTemplate(__DIR__ . '/assets/components/sidebar.html');
    }

    protected function getAllIngredientsHasJsList(): string
    {
        $allIngredients = array();
        $ingredientsResponse = $this->database->get("SELECT nom FROM ingredient");

        foreach ($ingredientsResponse as $ingredient) {
            array_push($allIngredients, $ingredient['nom']);
        }

        return json_encode($allIngredients);
    }
}


$index = new Index();
