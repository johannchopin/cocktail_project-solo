<?php

// INIT PAGEHANDLER ZONE
require_once __DIR__ . '/../../core/pageHandler.php';
// INIT PAGEHANDLER ZONE

class LoginPage extends PageHandler
{
    protected $hasPageErrors = FALSE;
    protected $errorMessage = "";

    public function __construct()
    {
        parent::__construct();

        if (isset($_POST['submit'])) {
            $this->logUser();
        }
    }

    public function pageRender(): string
    {
        return $this->getTemplate(__DIR__ . "/template.html", array(
            "__ERROR_MODAL__"     => $this->errorModalRender(),
        ));
    }

    protected function logUser(): void
    {
        if ($this->checkUserDataExistence()) {
            if ($this->isUser()) {
                $userData = $this->getUserData();

                $this->setCookie("token", $userData["token"]);
            } else {
                $this->hasPageErrors = TRUE;
                $this->errorMessage = "Not user";
            }
        } else {
            $this->hasPageErrors = TRUE;
            $this->errorMessage = "Please enter all required field !";
        }
    }

    protected function checkUserDataExistence(): bool
    {
        return isset($_POST["user_login"]) && isset($_POST["user_pwd"]) &&  trim($_POST["user_login"]) !== "" && trim($_POST["user_pwd"]) !== "";
    }

    protected function isUser(): bool
    {
        return $this->database->isPresent("SELECT * FROM client WHERE login = :userLogin AND password = :userPwd", array(
            "userLogin" => $_POST["user_login"],
            "userPwd"   => $_POST["user_pwd"],
        ));
    }

    protected function getUserData(): array
    {
        $userTokenResponse = $this->database->get("SELECT token, name, first_name FROM client WHERE login = :userLogin AND password = :userPwd", array(
            "userLogin" => $_POST["user_login"],
            "userPwd"   => $_POST["user_pwd"],
        ));

        return array(
            "name"       => $userTokenResponse[0]["name"],
            "firstName"  => $userTokenResponse[0]["first_name"],
            "token"      => $userTokenResponse[0]["token"],
        );
    }

    public function errorModalRender(): string
    {
        if ($this->hasPageErrors) {
            return $this->getTemplate(__DIR__ . "/../../assets/components/errorModal.html", array(
                "__MESSAGE__" => $this->errorMessage,
            ));
        }

        return "";
    }
}
