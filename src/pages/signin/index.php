<?php

// INIT PAGEHANDLER ZONE
require_once __DIR__ . '/../../core/pageHandler.php';
// INIT PAGEHANDLER ZONE

class SigninPage extends PageHandler
{
    protected $hasPageErrors = FALSE;
    protected $errorMessage = "";

    public function __construct()
    {
        parent::__construct();

        if (isset($_POST['submit'])) {
            $this->createUser();
        }
    }

    public function pageRender(): string
    {
        return $this->getTemplate(__DIR__ . "/template.html", array(
            "__ERROR_MODAL__"     => $this->errorModalRender(),
        ));
    }

    protected function createUser()
    {
        if ($this->checkUserDataExistence()) {
            $userToken = $this->getGeneratedToken();

            if ($this->isLoginUnique($_POST["user_login"])) {
                $this->setCookie("token", $userToken);

                $this->insertUserToDb($userToken);
            } else {
                $this->hasPageErrors = TRUE;
                $this->errorMessage = "the login is already used !";
            }
        } else {
            $this->hasPageErrors = TRUE;
            $this->errorMessage = "Please enter all required field !";
        }
    }

    protected function isLoginUnique(string $userLogin): bool
    {
        return !$this->database->isPresent("SELECT * FROM client WHERE login = :userLogin", array(
            "userLogin" => $userLogin,
        ));
    }

    protected function checkUserDataExistence(): bool
    {
        return isset($_POST["user_login"]) && isset($_POST["user_pwd"]) && isset($_POST["user_name"]) && isset($_POST["user_firstname"])
            &&  trim($_POST["user_login"]) !== "" && trim($_POST["user_pwd"]) !== "" && trim($_POST["user_name"]) !== "" && trim($_POST["user_firstname"]) !== "";
    }

    protected function insertUserToDb(string $token): void
    {
        $this->database->query("INSERT INTO client(token, login, password, name, first_name) VALUES(:token, :login, :password, :name, :first_name)", array(
            "token"      => $token,
            "login"      => $_POST["user_login"],
            "password"   => $_POST["user_pwd"],
            "name"       => $_POST["user_name"],
            "first_name" => $_POST["user_firstname"],
        ));
    }

    public function errorModalRender(): string
    {
        if ($this->hasPageErrors) {
            return $this->getTemplate(__DIR__ . "/../../assets/components/errorModal.html", array(
                "__MESSAGE__" => $this->errorMessage,
            ));
        }

        return "";
    }
}
