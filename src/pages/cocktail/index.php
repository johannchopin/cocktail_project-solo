<?php

// INIT PAGEHANDLER ZONE
require_once __DIR__ . '/../../core/pageHandler.php';
// INIT PAGEHANDLER ZONE

class CocktailPage extends PageHandler
{
    protected $cocktailId;
    protected $cocktail;

    public function __construct()
    {
        parent::__construct();

        if (isset($_GET["id"])) {
            $this->cocktailId = $_GET["id"];
        } else {
            $this->cocktailId = 1;
        }

        $cocktailResponse = $this->database->get("SELECT titre, ingredients, preparation FROM recette WHERE id = :id", array(
            "id" => $this->cocktailId,
        ));

        $this->cocktail = $cocktailResponse[0];
    }

    public function pageRender(): string
    {
        return $this->getTemplate(__DIR__ . "/template.html", array(
            "__COCKTAIL__" => $this->cocktailRender(),
        ));
    }

    protected function getRecipeIngredientsTag(string $recipeName)
    {
        return $this->database->get(
            "SELECT nom FROM ingredient WHERE 
            id IN (SELECT id_ingredient FROM est_compose WHERE 
            id_recette = (SELECT id FROM recette WHERE titre = :recipeName))",
            array(
                "recipeName" => $recipeName,
            )
        );
    }

    protected function recipeIngredientsTagRender(string $recipeName): string
    {
        $render = "";
        $recipeIngredientsTag = $this->getRecipeIngredientsTag($recipeName);

        foreach ($recipeIngredientsTag as $ingredientName) {
            $ingredientIdResponse = $this->database->get(
                "SELECT id FROM ingredient WHERE nom = :ingredientName ",
                array(
                    "ingredientName" => $ingredientName["nom"],
                )
            );
            $ingredientId = $ingredientIdResponse[0]["id"];

            $badgeRender = $this->getTemplate(__DIR__ . '/../../assets/components/badge.html', array(
                "__BADGE_TYPE__" => "dark",
                "__CONTENT__"    => $ingredientName["nom"],
            ));

            $render .= $this->linkRender("?recipe_contain=[{$ingredientId}]", $badgeRender);
        }

        return $render;
    }

    protected function getPathToImgFromCocktailName(string $cocktailName): string
    {
        $cocktailNameWithoutSpaces = str_replace(" ", "_", $cocktailName);
        $cocktailNameLowerCase = strtolower($cocktailNameWithoutSpaces);
        $imgName = ucfirst($cocktailNameLowerCase) . ".jpg";
        $pathToImg = "./assets/images/cocktails/" . $imgName;

        if (in_array($imgName, scandir(__DIR__ . "/../../assets/images/cocktails/"))) {
            return $pathToImg;
        }

        return "./assets/images/noImg.png";
    }

    public function cocktailRender(): string
    {
        return $this->getTemplate(__DIR__ . '/components/recipe.html', array(
            "__IMG_PATH__"      => $this->getPathToImgFromCocktailName($this->cocktail["titre"]),
            "__COCKTAIL_ID__"   => $this->cocktailId,
            "__NAME__"          => $this->cocktail["titre"],
            "__INGREDIENTS__"   => $this->ingredientsRender($this->cocktail["ingredients"]),
            "__PREPARATION__"   => $this->cocktail["preparation"],
            "__TAGS__"          => $this->recipeIngredientsTagRender($this->cocktail["titre"]),
        ));
    }

    protected function ingredientsRender(string $ingredients): string
    {
        return $this->listRender(explode("|", $ingredients));
    }
}
