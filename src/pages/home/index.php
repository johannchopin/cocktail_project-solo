<?php

// INIT PAGEHANDLER ZONE
require_once __DIR__ . '/../../core/pageHandler.php';
// INIT PAGEHANDLER ZONE

class HomePage extends PageHandler
{
    protected $recipes;

    public function __construct()
    {
        parent::__construct();

        $this->setRecipes();
    }

    public function pageRender(): string
    {
        return $this->getTemplate(__DIR__ . "/template.html", array(
            "__RECIPES__" => $this->recipesRender(),
        ));
    }

    protected function setRecipes()
    {

        if (isset($_POST["search_request"])) {
            $this->handleSearchRequest();
        }

        if (isset($_GET["recipe_contain"]) || isset($_GET["recipe_not_contain"])) {
            $this->recipes = $this->getMatchingRecipes();
            $this->sortRecipesByScore();
        } else {
            $this->recipes = $this->database->get("SELECT id, titre, ingredients, preparation FROM recette");
        }
    }

    protected function getMatchingRecipes(): array
    {
        if (isset($_GET["recipe_contain"]) && isset($_GET["recipe_not_contain"])) {
            $elmtsToMatchNumber = sizeof(json_decode($_GET["recipe_contain"])) + sizeof(json_decode($_GET["recipe_not_contain"]));

            $matchingRecipesThatContain = $this->getMatchingRecipesThatContain();
            $matchingRecipesThatNotContain = $this->getMatchingRecipesThatNotContain();

            foreach ($matchingRecipesThatContain as &$matchingRecipeThatContain) {
                foreach ($matchingRecipesThatNotContain as $matchingRecipeThatNotContain) {
                    if ($matchingRecipeThatContain["id"] === $matchingRecipeThatNotContain["id"]) {
                        $matchingRecipeThatContain["score"] = $matchingRecipeThatContain["score"] + 1;
                    }
                }
            }

            return $this->getRecipesWithPercentageScore($matchingRecipesThatContain, $elmtsToMatchNumber);
        } elseif (isset($_GET["recipe_contain"])) {
            $elmtsToMatchNumber = sizeof(json_decode($_GET["recipe_contain"]));

            return $this->getRecipesWithPercentageScore($this->getMatchingRecipesThatContain(), $elmtsToMatchNumber);
        } elseif (isset($_GET["recipe_not_contain"])) {
            $elmtsToMatchNumber = sizeof(json_decode($_GET["recipe_not_contain"]));

            return $this->getRecipesWithPercentageScore($this->getMatchingRecipesThatNotContain(), $elmtsToMatchNumber);
        }
    }

    protected function sortRecipesByScore(): void
    {
        usort($this->recipes, array($this, 'cmp'));
    }

    protected function cmp(array $a, array $b)
    {
        return  $b["score"] - $a["score"];
    }

    protected function getMatchingRecipesThatContain(): array
    {
        $matchingRecipesThatContain = array();
        $ingredientsToMatch = json_decode($_GET["recipe_contain"]);

        foreach ($ingredientsToMatch as $ingredientToMatch) {
            $matchingRecipesThatContainCurrentIngredient = $this->database->get("SELECT id, titre, ingredients, preparation FROM recette WHERE id IN
                (SELECT id_recette FROM est_compose WHERE id_ingredient = :ingredientId)", array(
                "ingredientId" => $ingredientToMatch,
            ));

            foreach ($matchingRecipesThatContainCurrentIngredient as $matchingRecipeThatContainCurrentIngredient) {
                if ($this->isRecipeInRecipes($matchingRecipesThatContain, $matchingRecipeThatContainCurrentIngredient)) {
                    foreach ($matchingRecipesThatContain as &$matchingRecipeThatContain) {
                        if ($matchingRecipeThatContain["id"] === $matchingRecipeThatContainCurrentIngredient["id"]) {
                            $matchingRecipeThatContain["score"] = $matchingRecipeThatContain["score"] + 1;
                        }
                    }
                } else {
                    $matchingRecipeThatContainCurrentIngredient["score"] = 1;

                    array_push($matchingRecipesThatContain, $matchingRecipeThatContainCurrentIngredient);
                }
            }
        }

        return $matchingRecipesThatContain;
    }

    protected function getMatchingRecipesThatNotContain(): array
    {
        $matchingRecipesThatNotContain = array();
        $ingredientsToMatch = json_decode($_GET["recipe_not_contain"]);

        foreach ($ingredientsToMatch as $idIngredient) {
            $matchingRecipesThatNotContainCurrentIngredient = $this->database->get("SELECT id, titre, ingredients, preparation FROM recette WHERE id NOT IN
                (SELECT id_recette FROM est_compose WHERE id_ingredient = :ingredientId)", array(
                "ingredientId" => $idIngredient,
            ));

            foreach ($matchingRecipesThatNotContainCurrentIngredient as $matchingRecipeThatNotContainCurrentIngredient) {
                if ($this->isRecipeInRecipes($matchingRecipesThatNotContain, $matchingRecipeThatNotContainCurrentIngredient)) {
                    foreach ($matchingRecipesThatNotContain as &$matchingRecipeThatContain) {
                        if ($matchingRecipeThatContain["id"] === $matchingRecipeThatNotContainCurrentIngredient["id"]) {
                            $matchingRecipeThatContain["score"] = $matchingRecipeThatContain["score"] + 1;
                        }
                    }
                } else {
                    $matchingRecipeThatNotContainCurrentIngredient["score"] = 1;

                    array_push($matchingRecipesThatNotContain, $matchingRecipeThatNotContainCurrentIngredient);
                }
            }
        }

        return $matchingRecipesThatNotContain;
    }

    protected function getRecipesWithPercentageScore(array $recipes, int $elmtsToMatchNumber): array
    {
        foreach ($recipes as &$recipe) {
            $recipe["score"] = ($recipe["score"] / $elmtsToMatchNumber) * 100;
        }

        return $recipes;
    }

    protected function isRecipeInRecipes(array $recipes, array $givenRecipe): bool
    {
        foreach ($recipes as $recipe) {
            if ($recipe["id"] === $givenRecipe["id"]) {
                return TRUE;
            }
        }

        return FALSE;
    }

    protected function handleSearchRequest(): void
    {
        $searchRequest = $_POST["search_request"] . " ";

        $filteredIngredients = $this->getFilteredIngredients($searchRequest);
        $filteredIngredientsId = $this->getFilteredIngredientsId($filteredIngredients);

        $this->redirectWithSearchRequest($filteredIngredientsId["toHave"], $filteredIngredientsId["toNotHave"]);
    }

    protected function redirectWithSearchRequest(array $ingredientIdToHave, array $ingredientIdToNotHave)
    {
        $ingredientIdToHaveAsJson = json_encode($ingredientIdToHave);
        $ingredientIdToNotHaveAsJson = json_encode($ingredientIdToNotHave);

        $searchRequestAsUrl = '';

        if (!empty($ingredientIdToHave)) {
            $searchRequestAsUrl .= "&recipe_contain={$ingredientIdToHaveAsJson}";
        }

        if (!empty($ingredientIdToNotHave)) {
            $searchRequestAsUrl .= "&recipe_not_contain={$ingredientIdToNotHaveAsJson}";
        }

        header("Location: ./?p=home" . $searchRequestAsUrl, TRUE, 302);
    }

    protected function getFilteredIngredientsId(array $filteredIngredients): array
    {
        return array(
            "toHave"    => $this->getIngredientsId($filteredIngredients["toHave"]),
            "toNotHave" => $this->getIngredientsId($filteredIngredients["toNotHave"]),
        );
    }

    protected function getIngredientsId(array $ingredientsName): array
    {
        $ingredientsId = array();

        foreach ($ingredientsName as $ingredientName) {
            array_push($ingredientsId, $this->getIngredientId($ingredientName));
        }

        return array_filter($ingredientsId, function ($var) {
            return !is_null($var);
        });
    }

    protected function getIngredientId(string $ingredientName)
    {
        $ingredientIdResponse = $this->database->get(
            "SELECT id FROM ingredient WHERE nom = :ingredientName ",
            array(
                "ingredientName" => $ingredientName,
            )
        );

        if (isset($ingredientIdResponse[0]["id"])) {
            return $ingredientIdResponse[0]["id"];
        }

        return NULL;
    }

    protected function getFilteredIngredients(string $searchRequest): array
    {
        $elementsInQuotesPattern = '/"(.*?)"/';
        $elementsInQuotesWithPlusPattern = '/\+"(.*?)"/';
        $elementsInQuotesWithMinusPattern = '/\-"(.*?)"/';
        $elementsStartWithPlusPattern = '/\+(\w+)/';
        $elementsStartWithMinusPattern = '/\-(\w+)/';

        // ---
        preg_match_all($elementsInQuotesWithPlusPattern, $searchRequest, $elementsInQuotesWithPlusResult);
        preg_match_all($elementsStartWithPlusPattern, $searchRequest, $elementsStartWithPlusResult);
        $elementsInQuotesWithPlus = $elementsInQuotesWithPlusResult[1];
        $elementsStartWithPlus = $elementsStartWithPlusResult[1];

        $ingredientsWithPlus = array_merge($elementsInQuotesWithPlus, $elementsStartWithPlus);
        $sanitizedIngredientsWithPlus = array_unique($ingredientsWithPlus, SORT_REGULAR);

        $searchRequest = Helper::removeElmtsInString($searchRequest, array_merge($elementsInQuotesWithPlusResult[0], $elementsStartWithPlusResult[0]));
        // ---

        // ---
        preg_match_all($elementsInQuotesWithMinusPattern, $searchRequest, $elementsInQuotesWithMinusResult);
        preg_match_all($elementsStartWithMinusPattern, $searchRequest, $elementsStartWithMinusResult);
        $elementsInQuotesWithMinus = $elementsInQuotesWithMinusResult[1];
        $elementsStartWithMinus = $elementsStartWithMinusResult[1];

        $ingredientsWithMinus = array_merge($elementsInQuotesWithMinus, $elementsStartWithMinus);
        $sanitizedIngredientsWithMinus = array_unique($ingredientsWithMinus, SORT_REGULAR);

        $searchRequest = Helper::removeElmtsInString($searchRequest, array_merge($elementsInQuotesWithMinusResult[0], $elementsStartWithMinusResult[0]));
        // ---

        // ---
        preg_match_all($elementsInQuotesPattern, $searchRequest, $elementsInQuotesResult);
        $elementsInQuotes = $elementsInQuotesResult[1];

        $searchRequest = Helper::removeElmtsInString($searchRequest, $elementsInQuotesResult[0]);
        // ---

        $ingredientsToHave = array_unique(array_merge($sanitizedIngredientsWithPlus, $elementsInQuotes, explode(" ", $searchRequest)));
        $ingredientsToNotHave = $sanitizedIngredientsWithMinus;

        return array(
            "toHave"    => $ingredientsToHave,
            "toNotHave" => $ingredientsToNotHave,
        );
    }

    protected function getRecipeIngredientsTag(string $recipeName)
    {
        return $this->database->get(
            "SELECT nom FROM ingredient WHERE 
            id IN (SELECT id_ingredient FROM est_compose WHERE 
            id_recette = (SELECT id FROM recette WHERE titre = :recipeName))",
            array(
                "recipeName" => $recipeName,
            )
        );
    }

    protected function recipeIngredientsTagRender(string $recipeName): string
    {
        $render = "";
        $recipeIngredientsTag = $this->getRecipeIngredientsTag($recipeName);

        foreach ($recipeIngredientsTag as $ingredientName) {
            $ingredientIdResponse = $this->database->get(
                "SELECT id FROM ingredient WHERE nom = :ingredientName ",
                array(
                    "ingredientName" => $ingredientName["nom"],
                )
            );
            $ingredientId = $ingredientIdResponse[0]["id"];

            $badgeRender = $this->getTemplate(__DIR__ . '/../../assets/components/badge.html', array(
                "__BADGE_TYPE__" => "dark",
                "__CONTENT__"    => $ingredientName["nom"],
            ));

            $render .= $this->linkRender("?recipe_contain=[{$ingredientId}]", $badgeRender);
        }

        return $render;
    }

    protected function recipesRender(): string
    {
        $render = "";

        foreach ($this->recipes as $recipe) {
            $render .= $this->getTemplate(__DIR__ . '/components/recipe.html', array(
                "__COCKTAIL_ID__"   => $recipe["id"],
                "__COCKTAIL_NAME__" => $recipe["titre"],
                "__TAGS__"          => $this->recipeIngredientsTagRender($recipe["titre"]),
                "__SCORE__"         => $this->recipeScoreRender($recipe),
            ));
        }

        return $render;
    }

    protected function recipeScoreRender(array $recipe): string
    {
        if (isset($recipe["score"])) {
            return $this->getTemplate(__DIR__ . '/components/recipeCardFooter.html', array(
                "__SCORE__" => $recipe["score"]
            ));
        }

        return "";
    }
}
